### <h2>Olá, eu sou o Caio César 👋</h2>

<div align="center">
  <a href="https://github.com/CaioCesarP">
  <img align="left" height="140em" src="https://github-readme-stats.vercel.app/api?username=CaioCesarP&show_icons=true&theme=tokyonight&include_all_commits=true&count_private=true"/>
</div>
  
<div >
  <img align="right" alt="Caio-pic" height="120" style="border-radius:100px;" src="https://cdn.discordapp.com/attachments/524055688540848128/901635038041477151/media.io_sRfmmZmX.gif">
</div>    
  
### <div> 
  <a href = "mail:caiocesarworks@gmail.com"><img align="center" src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/profile-caio-césar-link/" target="_blank"><img align="center" src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>
</div>


<div style="display: inline_block"><br>
  <img align="center" alt="Caio-React" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original.svg">
  <!--futuramente pretendo estudar PHP
  <img align="center" alt="Caio-php" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/php/php-plain.svg">-->
  <!--atualmente praticando o javascript-->
  <img align="center" title="ESTUDANDO" alt="Caio-Js" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
  <!--domínio considerado em HTML e CSS-->
  <img align="center" title="FINALIZADO" alt="Caio-HTML" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
  <img align="center" title="FINALIZADO" alt="Caio-CSS" height="30" width="40" src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
 
</div>
